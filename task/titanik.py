import pandas as pd
import numpy as np

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    q = ["Mr.", "Mrs.", "Miss."]
    df = get_titatic_dataframe()
    df['Name_l'] = df['Name'].str.lower()
    df['class_val'] = np.where(df['Name_l'].str.contains('miss.'), 'miss',
                      np.where(df['Name_l'].str.contains('mrs.'), 'mrs',
                      np.where(df['Name_l'].str.contains('mr.'), 'mr', 0)))
    
    medians = df.groupby(['class_val'])['Age'].median().astype(int).to_dict() #.to_numpy().astype(int)
    nulls = df.groupby(['class_val'])['Age'].apply(lambda x: x.isna().sum()).to_dict()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    ans = []

    d = {"Mr.": 'mr', "Mrs.": 'mrs', "Miss.": 'miss'}
    for i in q:
        s = d[i]
        if nulls[s]:
            tup = (i, nulls[s], medians[s])
            ans.append(tup)
    return ans
